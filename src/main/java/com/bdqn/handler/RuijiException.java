package com.bdqn.handler;

/**
 * @author:zyw
 * @create:2023-06-07 22:06
 */
public class RuijiException extends Exception {
    public RuijiException(String message) {
        super(message);
    }
}
