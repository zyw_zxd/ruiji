package com.bdqn.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.bdqn.entity.Setmeal;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 套餐 服务类
 * </p>
 *
 * @author zyw
 * @since 2023-06-07
 */
public interface SetmealService extends IService<Setmeal> {
    public IPage<Setmeal> findListPage(IPage<Setmeal> p, String name);
}
