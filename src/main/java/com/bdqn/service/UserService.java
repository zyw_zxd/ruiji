package com.bdqn.service;

import com.bdqn.entity.User;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户信息 服务类
 * </p>
 *
 * @author zyw
 * @since 2023-06-09
 */
public interface UserService extends IService<User> {

}
