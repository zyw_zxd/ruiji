package com.bdqn.service;

import com.bdqn.entity.DishFlavor;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 菜品口味关系表 服务类
 * </p>
 *
 * @author zyw
 * @since 2023-06-08
 */
public interface DishFlavorService extends IService<DishFlavor> {

}
