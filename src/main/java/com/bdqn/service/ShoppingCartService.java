package com.bdqn.service;

import com.bdqn.entity.ShoppingCart;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 购物车 服务类
 * </p>
 *
 * @author zyw
 * @since 2023-07-06
 */
public interface ShoppingCartService extends IService<ShoppingCart> {

}
