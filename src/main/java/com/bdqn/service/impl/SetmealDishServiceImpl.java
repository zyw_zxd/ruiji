package com.bdqn.service.impl;

import com.bdqn.entity.SetmealDish;
import com.bdqn.mapper.SetmealDishMapper;
import com.bdqn.service.SetmealDishService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 套餐菜品关系 服务实现类
 * </p>
 *
 * @author zyw
 * @since 2023-06-20
 */
@Service
public class SetmealDishServiceImpl extends ServiceImpl<SetmealDishMapper, SetmealDish> implements SetmealDishService {

}
