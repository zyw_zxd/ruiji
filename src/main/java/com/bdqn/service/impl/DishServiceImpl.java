package com.bdqn.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bdqn.entity.Dish;
import com.bdqn.mapper.DishMapper;
import com.bdqn.service.DishService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * <p>
 * 菜品管理 服务实现类
 * </p>
 *
 * @author zyw
 * @since 2023-06-07
 */
@Service
public class DishServiceImpl extends ServiceImpl<DishMapper, Dish> implements DishService {
    @Resource
    private DishMapper dishMapper;
    @Override
    public IPage<Dish> findDishList(Page<Dish> page,@Param("name") String name) {
        return dishMapper.findDishList(page,name);
    }
}
