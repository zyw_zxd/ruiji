package com.bdqn.service.impl;

import com.bdqn.entity.OrderDetail;
import com.bdqn.mapper.OrderDetailMapper;
import com.bdqn.service.OrderDetailService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 订单明细表 服务实现类
 * </p>
 *
 * @author zyw
 * @since 2023-07-06
 */
@Service
public class OrderDetailServiceImpl extends ServiceImpl<OrderDetailMapper, OrderDetail> implements OrderDetailService {

}
