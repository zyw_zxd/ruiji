package com.bdqn.service.impl;

import com.bdqn.entity.AddressBook;
import com.bdqn.mapper.AddressBookMapper;
import com.bdqn.service.AddressBookService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 地址管理 服务实现类
 * </p>
 *
 * @author zyw
 * @since 2023-06-16
 */
@Service
public class AddressBookServiceImpl extends ServiceImpl<AddressBookMapper, AddressBook> implements AddressBookService {

}
