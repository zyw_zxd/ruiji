package com.bdqn.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.bdqn.entity.Setmeal;
import com.bdqn.mapper.SetmealMapper;
import com.bdqn.service.SetmealService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * <p>
 * 套餐 服务实现类
 * </p>
 *
 * @author zyw
 * @since 2023-06-07
 */
@Service
public class SetmealServiceImpl extends ServiceImpl<SetmealMapper, Setmeal> implements SetmealService {
    @Resource
    private SetmealMapper setmealMapper;
    @Override
    public IPage<Setmeal> findListPage(IPage<Setmeal> p, String name) {
        return setmealMapper.findListPage(p,name);
    }
}
