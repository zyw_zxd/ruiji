package com.bdqn.service.impl;

import com.bdqn.entity.Employee;
import com.bdqn.mapper.EmployeeMapper;
import com.bdqn.service.EmployeeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 员工信息 服务实现类
 * </p>
 *
 * @author zyw
 * @since 2023-06-07
 */
@Service
public class EmployeeServiceImpl extends ServiceImpl<EmployeeMapper, Employee> implements EmployeeService {

}
