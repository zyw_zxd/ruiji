package com.bdqn.service.impl;

import com.bdqn.entity.ShoppingCart;
import com.bdqn.mapper.ShoppingCartMapper;
import com.bdqn.service.ShoppingCartService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 购物车 服务实现类
 * </p>
 *
 * @author zyw
 * @since 2023-07-06
 */
@Service
public class ShoppingCartServiceImpl extends ServiceImpl<ShoppingCartMapper, ShoppingCart> implements ShoppingCartService {

}
