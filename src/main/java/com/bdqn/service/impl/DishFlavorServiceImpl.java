package com.bdqn.service.impl;

import com.bdqn.entity.DishFlavor;
import com.bdqn.mapper.DishFlavorMapper;
import com.bdqn.service.DishFlavorService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 菜品口味关系表 服务实现类
 * </p>
 *
 * @author zyw
 * @since 2023-06-08
 */
@Service
public class DishFlavorServiceImpl extends ServiceImpl<DishFlavorMapper, DishFlavor> implements DishFlavorService {

}
