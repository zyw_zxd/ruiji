package com.bdqn.service.impl;

import com.bdqn.entity.Category;
import com.bdqn.mapper.CategoryMapper;
import com.bdqn.service.CategoryService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 菜品及套餐分类 服务实现类
 * </p>
 *
 * @author zyw
 * @since 2023-06-07
 */
@Service
public class CategoryServiceImpl extends ServiceImpl<CategoryMapper, Category> implements CategoryService {

}
