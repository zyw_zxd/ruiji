package com.bdqn.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.bdqn.entity.*;
import com.bdqn.handler.RuijiException;
import com.bdqn.mapper.OrdersMapper;
import com.bdqn.service.AddressBookService;
import com.bdqn.service.OrdersService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bdqn.service.ShoppingCartService;
import com.bdqn.service.UserService;
import com.bdqn.utils.ThreadLocalUtil;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

/**
 * <p>
 * 订单表 服务实现类
 * </p>
 *
 * @author zyw
 * @since 2023-07-06
 */
@Service
public class OrdersServiceImpl extends ServiceImpl<OrdersMapper, Orders> implements OrdersService {
    @Resource
    private ShoppingCartService shoppingCartService;
    @Resource
    private UserService userService;
    @Resource
    private AddressBookService addressBookService;
    @Resource
    private OrderDetailServiceImpl orderDetailService;
    @Transactional
    @Override
    public void submit(Orders orders) throws RuijiException {
        //获取用户id
        Long userId = ThreadLocalUtil.getID();
        //查看订单数据
        LambdaQueryWrapper<ShoppingCart> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(ShoppingCart::getUserId,userId);
        List<ShoppingCart> shoppingCartList = shoppingCartService.list(lambdaQueryWrapper);
        if (shoppingCartList==null || shoppingCartList.size()==0){
            throw new RuijiException("购物车不空,不能下单!");
        }
        //查询用户
        User user = userService.getById(userId);
        //查询地址
        AddressBook address = addressBookService.getById(orders.getAddressBookId());
        if (address==null ){
            throw new RuijiException("地址不能为空,不能下单!");
        }


        //保存订单
        long orderId = IdWorker.getId();//获取订单号
        //将购物车中的数据封装到购物详情中
        AtomicInteger amount= new AtomicInteger(0);
        List<OrderDetail> orderDetailList=  shoppingCartList.stream().map(item->{
          OrderDetail orderDetail = new OrderDetail();
          orderDetail.setOrderId(orderId);
          orderDetail.setName(item.getName());
          orderDetail.setDishFlavor(item.getDishFlavor());
          orderDetail.setDishId(item.getDishId());
          orderDetail.setNumber(item.getNumber());
          orderDetail.setImage(item.getImage());
          orderDetail.setSetmealId(item.getSetmealId());
          orderDetail.setAmount(item.getAmount());//价格
          amount.addAndGet(item.getAmount().multiply(new BigDecimal(item.getNumber())).intValue());
          return orderDetail;
      }).collect(Collectors.toList());
        orders.setId(orderId);
        orders.setNumber(String.valueOf(orderId));
        orders.setStatus(2);
        orders.setAmount(new BigDecimal(amount.get()));
        orders.setUserId(userId);
        orders.setUserName(user.getName());
        orders.setConsignee(address.getConsignee());
        orders.setPhone(address.getPhone());
        orders.setAddress(address.getDetail());
        orders.setOrderTime(new Date());
        orders.setCheckoutTime(new Date());
        boolean save = this.save(orders);
        //保存订单详情
        orderDetailService.saveBatch(orderDetailList);
        //情况购物车
        shoppingCartService.remove(lambdaQueryWrapper);
    }
}
