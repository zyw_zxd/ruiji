package com.bdqn.service.impl;

import com.bdqn.entity.User;
import com.bdqn.mapper.UserMapper;
import com.bdqn.service.UserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户信息 服务实现类
 * </p>
 *
 * @author zyw
 * @since 2023-06-09
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {

}
