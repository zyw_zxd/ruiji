package com.bdqn.service;

import com.bdqn.entity.Category;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 菜品及套餐分类 服务类
 * </p>
 *
 * @author zyw
 * @since 2023-06-07
 */
public interface CategoryService extends IService<Category> {

}
