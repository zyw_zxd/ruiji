package com.bdqn.service;

import com.bdqn.entity.OrderDetail;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 订单明细表 服务类
 * </p>
 *
 * @author zyw
 * @since 2023-07-06
 */
public interface OrderDetailService extends IService<OrderDetail> {

}
