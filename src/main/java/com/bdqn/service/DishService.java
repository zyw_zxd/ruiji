package com.bdqn.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bdqn.entity.Dish;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 菜品管理 服务类
 * </p>
 *
 * @author zyw
 * @since 2023-06-07
 */
public interface DishService extends IService<Dish> {
    public IPage<Dish> findDishList(Page<Dish> page, String name);
}
