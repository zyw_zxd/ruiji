package com.bdqn.service;

import com.bdqn.entity.SetmealDish;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 套餐菜品关系 服务类
 * </p>
 *
 * @author zyw
 * @since 2023-06-20
 */
public interface SetmealDishService extends IService<SetmealDish> {

}
