package com.bdqn.service;

import com.bdqn.entity.Orders;
import com.baomidou.mybatisplus.extension.service.IService;
import com.bdqn.handler.RuijiException;

/**
 * <p>
 * 订单表 服务类
 * </p>
 *
 * @author zyw
 * @since 2023-07-06
 */
public interface OrdersService extends IService<Orders> {
    public void submit(Orders orders) throws RuijiException;
}
