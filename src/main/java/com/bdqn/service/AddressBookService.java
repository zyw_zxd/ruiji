package com.bdqn.service;

import com.bdqn.entity.AddressBook;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 地址管理 服务类
 * </p>
 *
 * @author zyw
 * @since 2023-06-16
 */
public interface AddressBookService extends IService<AddressBook> {

}
