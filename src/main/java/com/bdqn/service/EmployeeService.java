package com.bdqn.service;

import com.bdqn.entity.Employee;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 员工信息 服务类
 * </p>
 *
 * @author zyw
 * @since 2023-06-07
 */
public interface EmployeeService extends IService<Employee> {

}
