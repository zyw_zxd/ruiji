package com.bdqn.dto;

import com.bdqn.entity.Setmeal;
import com.bdqn.entity.SetmealDish;
import lombok.Data;
import java.util.List;

@Data
public class SetmealDto extends Setmeal {

    private List<SetmealDish> setmealDishes;

    private String categoryName;
}
