package com.bdqn.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 菜品口味关系表 前端控制器
 * </p>
 *
 * @author zyw
 * @since 2023-06-08
 */
@RestController
@RequestMapping("/dish-flavor")
public class DishFlavorController {

}

