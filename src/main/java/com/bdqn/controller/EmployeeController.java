package com.bdqn.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bdqn.entity.Employee;
import com.bdqn.service.EmployeeService;
import com.bdqn.utils.R;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.DigestUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * <p>
 * 员工信息 前端控制器
 * </p>
 *
 * @author zyw
 * @since 2023-06-07
 */
@Slf4j
@RestController
@RequestMapping("/employee")
public class EmployeeController {
    @Resource
    private EmployeeService employeeService;
    @PostMapping("/login")
    public R login(HttpSession session, @RequestBody Employee employee){
        String password = employee.getPassword();
        password= DigestUtils.md5DigestAsHex(password.getBytes());
        QueryWrapper<Employee> wrapper = new QueryWrapper<>();
        wrapper.eq("username",employee.getUsername());
        wrapper.eq("password",password);
        Employee emp = employeeService.getOne(wrapper);
        if (emp==null){
            return R.error("用户名或者密码不正确!");
        }
        if (emp.getStatus()!=1){
            return R.error("用户已禁用!");
        }
        session.setAttribute("empID",emp.getId());
        return R.success(emp);

    }
    @PostMapping("/logout")
    public R logOut(HttpSession session){
        session.invalidate();
        return R.success("退出");
    }
    @PostMapping
    public R add(HttpSession session,@RequestBody Employee employee){
        //设置初始密码
        employee.setPassword( DigestUtils.md5DigestAsHex("123".getBytes()));
        boolean save = employeeService.save(employee);
        if (save){
            return R.success("添加成功");
        }else{
            return R.error("添加失败");
        }
    }
    @PutMapping
    public R update(@RequestBody Employee employee){
        boolean b = employeeService.updateById(employee);
        if (b){
            return R.success("更新成功");
        }else{
            return R.error("更新失败");
        }

    }

    @GetMapping("/page")
    public R<Page> page(int page,Integer pageSize,String name){
        log.info(page+"\t"+pageSize+"\t"+name);
        Page<Employee> p = new Page<>(page, pageSize);
        QueryWrapper<Employee> wrapper = new QueryWrapper<>();
        if (name!=null) {
            wrapper.like("name", name);
        }
        wrapper.orderByDesc("update_time");
        employeeService.page(p,wrapper);
        return R.success(p);
    }
    @GetMapping("/{id}")
    public R getById(@PathVariable Long id){
        log.info("id:"+id);
        Employee employee = employeeService.getById(id);
        return R.success(employee);
    }
}

