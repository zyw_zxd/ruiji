package com.bdqn.controller;


import com.bdqn.entity.Orders;
import com.bdqn.handler.RuijiException;
import com.bdqn.service.OrdersService;
import com.bdqn.utils.R;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * <p>
 * 订单表 前端控制器
 * </p>
 *
 * @author zyw
 * @since 2023-07-06
 */
@RestController
@RequestMapping("/order")
public class OrdersController {
    @Resource
    private OrdersService orderService;
    @PostMapping("/submit")
    public R save(@RequestBody Orders orders) throws RuijiException {
        orderService.submit(orders);
        return R.success("下单成功");
    }
}

