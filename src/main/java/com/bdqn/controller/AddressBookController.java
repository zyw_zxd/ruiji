package com.bdqn.controller;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.bdqn.entity.AddressBook;
import com.bdqn.service.AddressBookService;
import com.bdqn.utils.R;
import com.bdqn.utils.ThreadLocalUtil;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * <p>
 * 地址管理 前端控制器
 * </p>
 * @author zyw
 * @since 2023-06-16
 */
@RestController
@RequestMapping("/addressBook")
public class AddressBookController {
    @Resource
    private AddressBookService addressBookService;
    @GetMapping("/default")
    public R findDefault(){
        LambdaQueryWrapper<AddressBook> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(AddressBook::getUserId,ThreadLocalUtil.getID());
        lambdaQueryWrapper.eq(AddressBook::getIsDefault,1);
        lambdaQueryWrapper.eq(AddressBook::getIsDeleted,0);
        AddressBook addressBook = addressBookService.getOne(lambdaQueryWrapper);
        return R.success(addressBook);
    }
    @PutMapping("/default")
    public R updateDefault(@RequestBody AddressBook addressBook,HttpSession session){
        long  userId = (long)session.getAttribute("user");
        QueryWrapper<AddressBook> wrapper = new QueryWrapper<>();
        addressBook.setIsDefault(false);
        wrapper.eq("user_id",userId);
        addressBookService.update(addressBook,wrapper);
        addressBook.setIsDefault(true);
        addressBookService.updateById(addressBook);
        return R.success("修改默认成功");
    }

    @GetMapping("/list")
    public R findList(HttpSession session){
        QueryWrapper<AddressBook> wrapper = new QueryWrapper<>();
        wrapper.eq("user_id",session.getAttribute("user"));
        wrapper.orderByDesc("update_time");
        List<AddressBook> bookList = addressBookService.list(wrapper);
        return R.success(bookList);
    }

    @PostMapping
    public R  save(@RequestBody AddressBook addressBook, HttpSession session){
        Long userId = (Long) session.getAttribute("user");
        addressBook.setUserId(userId);
        addressBookService.save(addressBook);
        return R.success("保存成功!");
    }

}

