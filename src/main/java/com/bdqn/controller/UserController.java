package com.bdqn.controller;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.bdqn.entity.User;
import com.bdqn.service.UserService;
import com.bdqn.utils.R;
import com.bdqn.utils.SMSUtils;
import com.bdqn.utils.SmsUtil;
import com.bdqn.utils.ValidateCodeUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.net.ssl.HttpsURLConnection;
import javax.servlet.http.HttpSession;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * <p>
 * 用户信息 前端控制器
 * </p>
 *
 * @author zyw
 * @since 2023-06-09
 */
@Slf4j
@RestController
@RequestMapping("/user")
public class UserController {
    @Resource
    private UserService userService;
    @Resource
    private RedisTemplate redisTemplate;
    @PostMapping("/sendMsg")
    public R sendMsg( HttpSession session,@RequestBody User user){
        String phone = user.getPhone();
        if (phone!=null){
            //生成验证码
            String code = ValidateCodeUtils.generateValidateCode(6).toString();
            /*
            前后端不分类可以将验证码存入session中
             */
            // SmsUtil.sendMessage(phone,code);
            //session.setAttribute("17709503902","1234");
            /*
            前后端分类需要用redis保存
             */
            redisTemplate.opsForValue().set(phone,code,5, TimeUnit.MINUTES);
            return R.success("发送成功");
        }
        return R.error("发送失败");
    }
    @PostMapping("/login")
    public R login(HttpSession session,@RequestBody Map<String,String> map){
        String phone = map.get("phone");
        String code = map.get("code");
        log.info("手机号:"+phone+"验证:"+code);
      //  Object user1 = session.getAttribute(phone);
        if (redisTemplate.opsForValue().get(phone)!=null&&redisTemplate.opsForValue().get(phone).equals(code)){
            LambdaQueryWrapper<User> wrapper = new LambdaQueryWrapper<>();
            wrapper.eq(User::getPhone,phone);
            User user = userService.getOne(wrapper);
            if (user==null){
                user= new User();
                user.setPhone(phone);
                user.setStatus(1);
                userService.save(user);
            }
            session.setAttribute("user",user.getId());
            redisTemplate.delete(phone);
            return R.success(user);
        }
        return R.error("登录失败");
    }
}

