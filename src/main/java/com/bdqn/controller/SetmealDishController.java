package com.bdqn.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 套餐菜品关系 前端控制器
 * </p>
 *
 * @author zyw
 * @since 2023-06-20
 */
@RestController
@RequestMapping("/setmeal-dish")
public class SetmealDishController {

}

