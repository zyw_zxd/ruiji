package com.bdqn.controller;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bdqn.entity.Category;
import com.bdqn.entity.Dish;
import com.bdqn.entity.Employee;
import com.bdqn.entity.Setmeal;
import com.bdqn.handler.RuijiException;
import com.bdqn.service.CategoryService;
import com.bdqn.service.DishService;
import com.bdqn.service.SetmealService;
import com.bdqn.utils.R;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 菜品及套餐分类 前端控制器
 * </p>
 *
 * @author zyw
 * @since 2023-06-07
 */
@RestController
@RequestMapping("/category")
@Slf4j
public class CategoryController {
    @Resource
    private CategoryService categoryService;
    @Resource
    private DishService dishService;
    @Resource
    private SetmealService setmealService;
    @GetMapping("/list")
    public R findList(Category category){
        LambdaQueryWrapper<Category> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(category.getType()!=null,Category::getType,category.getType());
        wrapper.orderByDesc(Category::getSort);
        List<Category> categoryList = categoryService.list(wrapper);
        return R.success(categoryList);
    }
    @PostMapping
    public R save(@RequestBody Category category){
        categoryService.save(category);
        return R.success("保存成功");
    }
    @GetMapping("/page")
    public R<Page> page(int page, Integer pageSize){
        log.info(page+"\t"+pageSize+"\t");
        Page<Category> p = new Page<>(page, pageSize);
        QueryWrapper<Category> wrapper = new QueryWrapper<>();
        wrapper.orderByDesc("update_time");
        categoryService.page(p,wrapper);
        return R.success(p);
    }
    @DeleteMapping
    public R delete(Long ids) throws RuijiException {
        QueryWrapper<Setmeal> wrapper = new QueryWrapper<>();
        wrapper.eq("category_id",ids);
        int count = setmealService.count(wrapper);
        if (count>0){
           throw new RuijiException("与套餐关联无法删除");
        }
        QueryWrapper<Dish> query = new QueryWrapper<>();
        wrapper.eq("category_id",ids);
        int coutDish = dishService.count(query);
        if (count>0){
            throw new RuijiException("与套餐关联无法删除");
        }
        boolean b = categoryService.removeById(ids);
        return R.success("删除成功!");
    }
    @GetMapping("/{id}")
    public R getById(@PathVariable Long id){
        log.info("id:"+id);
        Category category = categoryService.getById(id);
        return R.success(category);
    }
    @PutMapping
    public R update(@RequestBody Category category){
        boolean b = categoryService.updateById(category);
        if (b){
            return R.success("更新成功");
        }else{
            return R.error("更新失败");
        }

    }
}

