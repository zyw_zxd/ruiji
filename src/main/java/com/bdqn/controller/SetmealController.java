package com.bdqn.controller;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bdqn.dto.SetmealDto;
import com.bdqn.entity.Dish;
import com.bdqn.entity.Setmeal;
import com.bdqn.entity.SetmealDish;
import com.bdqn.service.SetmealDishService;
import com.bdqn.service.SetmealService;
import com.bdqn.utils.R;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * 套餐 前端控制器
 * @author zyw
 * @since 2023-06-07
 */
@Slf4j
@RestController
@RequestMapping("/setmeal")
public class SetmealController {
    @Resource
    private SetmealService setmealService;
    @Resource
    private SetmealDishService setmealDishService;
    @GetMapping("/list")
    @Cacheable(value = "setmealCache",key = "#setmeal.categoryId+'_'+#setmeal.status")
    public R findList(Setmeal setmeal){
        LambdaQueryWrapper<Setmeal> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(setmeal.getStatus()!=null,Setmeal::getStatus,setmeal.getStatus());
        wrapper.eq(setmeal.getCategoryId()!=null,Setmeal::getCategoryId,setmeal.getCategoryId());
        wrapper.orderByDesc(Setmeal::getUpdateTime);
        List<Setmeal> setmealList = setmealService.list(wrapper);
        return R.success(setmealList);
    }
    @GetMapping("/page")
    public R findList(String name,Integer page,Integer pageSize){
        Page<Setmeal> p = new Page<>(page, pageSize);
        IPage<Setmeal> listPage = setmealService.findListPage(p, name);
        return R.success(listPage);
    }

    @PostMapping
    @Transactional
    @CacheEvict(value = "setmealCache",allEntries = true)
    public R save(@RequestBody SetmealDto setmealDto){
        log.info("对象信息："+setmealDto);
        setmealService.save(setmealDto);
        log.info("对象信息："+setmealDto.getId());
        List<SetmealDish> setmealDishes = setmealDto.getSetmealDishes();
        for (SetmealDish setmealDish : setmealDishes) {
            setmealDish.setSetmealId(setmealDto.getId());
        }
        setmealDishService.saveBatch(setmealDishes);
        return R.success("保存成功");
    }

}

