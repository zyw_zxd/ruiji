package com.bdqn.controller;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.bdqn.entity.ShoppingCart;
import com.bdqn.service.ShoppingCartService;
import com.bdqn.utils.R;
import com.bdqn.utils.ThreadLocalUtil;
import com.sun.prism.impl.BaseContext;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 购物车 前端控制器
 * </p>
 *
 * @author zyw
 * @since 2023-07-06
 */
@RestController
@RequestMapping("/shoppingCart")
public class ShoppingCartController {
    @Resource
    private ShoppingCartService shoppingCartService;
    @DeleteMapping("/clean")
    public R clean(){
        LambdaQueryWrapper<ShoppingCart> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(ShoppingCart::getUserId,ThreadLocalUtil.getID());
        shoppingCartService.remove(lambdaQueryWrapper);
        return R.success("清空购物车");
    }
    @GetMapping("/list")
    public R findList(){
        LambdaQueryWrapper<ShoppingCart> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(ShoppingCart::getUserId,ThreadLocalUtil.getID());
        List<ShoppingCart> cartList = shoppingCartService.list(lambdaQueryWrapper);
        return R.success(cartList);
    }
    @PostMapping("/add")
    public R addCart(@RequestBody ShoppingCart shoppingCart){
        //添加用户id
        Long userId = ThreadLocalUtil.getID();
        shoppingCart.setUserId(userId);
        //查看菜品是否存在
        Long dishId = shoppingCart.getDishId();
        LambdaQueryWrapper<ShoppingCart> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(ShoppingCart::getUserId,userId);
        if (dishId!=null){
            lambdaQueryWrapper.eq(ShoppingCart::getDishId,dishId);
        }else{
            lambdaQueryWrapper.eq(ShoppingCart::getSetmealId,shoppingCart.getSetmealId());
        }
        ShoppingCart cart = shoppingCartService.getOne(lambdaQueryWrapper);
        if (cart!=null){
            cart.setNumber(cart.getNumber()+1);
            shoppingCartService.updateById(cart);
        }else{
            shoppingCart.setNumber(1);
            shoppingCartService.save(shoppingCart);
            cart=shoppingCart;
        }
        //保存
        return R.success(cart);

    }
}

