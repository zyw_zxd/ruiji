package com.bdqn.controller;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bdqn.dto.DishDto;
import com.bdqn.entity.Category;
import com.bdqn.entity.Dish;
import com.bdqn.entity.DishFlavor;
import com.bdqn.entity.Setmeal;
import com.bdqn.handler.RuijiException;
import com.bdqn.service.CategoryService;
import com.bdqn.service.DishFlavorService;
import com.bdqn.service.DishService;
import com.bdqn.utils.R;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * <p>
 * 菜品管理 前端控制器
 * </p>
 *
 * @author zyw
 * @since 2023-06-07
 */
@Slf4j
@RestController
@RequestMapping("/dish")
public class DishController {
    @Resource
    private DishService dishService;
    @Resource
    private DishFlavorService dishFlavorService;
    @Resource
    private CategoryService categoryService;
    @Resource
    private RedisTemplate redisTemplate;
    @GetMapping("/list")
    public R<List<DishDto>> findList(Dish dish){
        List<DishDto> dishDtoList=null;
        String key="dish_"+dish.getCategoryId()+"_"+dish.getStatus();
        dishDtoList= (List<DishDto>) redisTemplate.opsForValue().get(key);
        if (dishDtoList!=null){
            return R.success(dishDtoList);
        }
        LambdaQueryWrapper<Dish> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(Dish::getStatus,1);
        wrapper.eq(dish.getCategoryId()!=null,Dish::getCategoryId,dish.getCategoryId());
        wrapper.orderByDesc(Dish::getSort);
        List<Dish> dishList = dishService.list(wrapper);
        dishDtoList = dishList.stream().map((item) -> {
            DishDto dishDto = new DishDto();
            BeanUtils.copyProperties(item, dishDto);
            Long categoryId = item.getCategoryId();
            Category category = categoryService.getById(categoryId);
            dishDto.setCategoryName(category.getName());
            Long dishId = item.getId();//获取菜品id
            LambdaQueryWrapper<DishFlavor> lambdaQueryWrapper = new LambdaQueryWrapper<>();
            lambdaQueryWrapper.eq(DishFlavor::getDishId, dishId);
            List<DishFlavor> flavorList = dishFlavorService.list(lambdaQueryWrapper);
            dishDto.setFlavors(flavorList);
            return dishDto;
        }).collect(Collectors.toList());
        redisTemplate.opsForValue().set(key,dishDtoList,10, TimeUnit.DAYS);
        return R.success(dishDtoList);
    }

    @PostMapping
    @Transactional
    public R save(@RequestBody DishDto dishDto){
        dishService.save(dishDto);
        Long id = dishDto.getId();
        for (DishFlavor flavor : dishDto.getFlavors()) {
            flavor.setDishId(id);
        }
        dishFlavorService.saveBatch(dishDto.getFlavors());
        //清理缓存
        String key="dish_"+dishDto.getCategoryId()+"_1";
        redisTemplate.delete(key);
        return R.success("保存成功");
    }
    @GetMapping("/page")
    public R<Page> page(String name,int page, Integer pageSize){
        log.info(page+"\t"+pageSize+"\t"+name);
        Page<Dish> p = new Page<>(page, pageSize);
        QueryWrapper<Dish> wrapper = new QueryWrapper<>();
        if (name!=null) {
            wrapper.like("name", name);
        }
        wrapper.orderByDesc("update_time");
        dishService.findDishList(p,name);
        return R.success(p);
    }
    @GetMapping("/{id}")
    public R getById(@PathVariable Long id){
        log.info("id:"+id);
        Dish dish = dishService.getById(id);
        DishDto dishDto = new DishDto();
        BeanUtils.copyProperties(dish,dishDto);
        QueryWrapper<DishFlavor> wrapper = new QueryWrapper<>();
        wrapper.eq("dishId",id);
        List<DishFlavor> flavorList = dishFlavorService.list(wrapper);
        dishDto.setFlavors(flavorList);
        return R.success(dishDto);
    }
    @PutMapping
    public R update(@RequestBody DishDto dishDto){
        dishService.updateById(dishDto);
        Long id = dishDto.getId();
        for (DishFlavor flavor : dishDto.getFlavors()) {
            flavor.setDishId(id);
        }
        //更新口味
        List<DishFlavor> flavors = dishDto.getFlavors();
        for (DishFlavor flavor : flavors) {
            dishFlavorService.updateById(flavor);
        }
        //清理缓存
        String key="dish_"+dishDto.getCategoryId()+"_1";
        redisTemplate.delete(key);
        return R.success("保存成功");

    }
    @DeleteMapping
    public R delete(Long ids) throws RuijiException {
        boolean b = dishService.removeById(ids);
        return R.success("删除成功!");
    }
}

