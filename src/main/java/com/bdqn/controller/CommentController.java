package com.bdqn.controller;

import com.bdqn.utils.R;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpRequest;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URI;
import java.util.UUID;

/**
 * @author:zyw
 * @create:2023-06-08 15:15
 */
@RestController
@Slf4j
@RequestMapping("/common")
public class CommentController {
    String baseUrl="";
    @PostMapping("/upload")
    public R  upload(MultipartFile file) {
        baseUrl = System.getProperty("user.dir")+"\\images\\";
        File file1 = new File(baseUrl);
        if (!file1.exists()){
            file1.mkdir();
        }
        log.info("路径:"+baseUrl);
        String originalFilename = file.getOriginalFilename();
        //后缀
        String name = originalFilename.substring(originalFilename.lastIndexOf("."));
        String fileName = UUID.randomUUID().toString()+name;
        try {
            file.transferTo(new File(baseUrl+fileName));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return R.success(fileName);
    }
    @GetMapping("/download")
    public void  downLoad(String name, HttpServletResponse response)  {
        baseUrl = System.getProperty("user.dir")+"\\images\\";
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(baseUrl + name);

        byte[] b=new byte[1024];
        int len=0;
        ServletOutputStream outputStream = response.getOutputStream();
        response.setContentType("image/jpeg");
        while ((len=fis.read(b))!=-1){
            outputStream.write(b,0,len);
            outputStream.flush();
        }
            outputStream.close();
            fis.close();
        } catch (Exception e) {
            e.printStackTrace();
        }


    }
}
