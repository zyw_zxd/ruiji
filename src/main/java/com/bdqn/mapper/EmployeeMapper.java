package com.bdqn.mapper;

import com.bdqn.entity.Employee;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 员工信息 Mapper 接口
 * </p>
 *
 * @author zyw
 * @since 2023-06-07
 */
public interface EmployeeMapper extends BaseMapper<Employee> {

}
