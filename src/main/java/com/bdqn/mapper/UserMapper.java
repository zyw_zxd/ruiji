package com.bdqn.mapper;

import com.bdqn.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户信息 Mapper 接口
 * </p>
 *
 * @author zyw
 * @since 2023-06-09
 */
public interface UserMapper extends BaseMapper<User> {

}
