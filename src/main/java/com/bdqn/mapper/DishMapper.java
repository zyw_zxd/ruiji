package com.bdqn.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bdqn.entity.Dish;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 菜品管理 Mapper 接口
 * </p>
 *
 * @author zyw
 * @since 2023-06-07
 */
public interface DishMapper extends BaseMapper<Dish> {
    public IPage<Dish> findDishList(Page<Dish> page, String name);

}
