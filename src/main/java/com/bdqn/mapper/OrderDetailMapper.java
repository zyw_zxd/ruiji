package com.bdqn.mapper;

import com.bdqn.entity.OrderDetail;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 订单明细表 Mapper 接口
 * </p>
 *
 * @author zyw
 * @since 2023-07-06
 */
public interface OrderDetailMapper extends BaseMapper<OrderDetail> {

}
