package com.bdqn.mapper;

import com.bdqn.entity.Orders;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 订单表 Mapper 接口
 * </p>
 *
 * @author zyw
 * @since 2023-07-06
 */
public interface OrdersMapper extends BaseMapper<Orders> {

}
