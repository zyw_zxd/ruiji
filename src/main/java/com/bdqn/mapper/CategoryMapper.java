package com.bdqn.mapper;

import com.bdqn.entity.Category;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 菜品及套餐分类 Mapper 接口
 * </p>
 *
 * @author zyw
 * @since 2023-06-07
 */
public interface CategoryMapper extends BaseMapper<Category> {

}
