package com.bdqn.mapper;

import com.bdqn.entity.ShoppingCart;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 购物车 Mapper 接口
 * </p>
 *
 * @author zyw
 * @since 2023-07-06
 */
public interface ShoppingCartMapper extends BaseMapper<ShoppingCart> {

}
