package com.bdqn.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.bdqn.entity.Setmeal;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 套餐 Mapper 接口
 * </p>
 *
 * @author zyw
 * @since 2023-06-07
 */
public interface SetmealMapper extends BaseMapper<Setmeal> {
    public IPage<Setmeal> findListPage(IPage<Setmeal> p,String name);
}
