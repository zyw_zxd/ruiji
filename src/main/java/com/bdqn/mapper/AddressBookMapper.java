package com.bdqn.mapper;

import com.bdqn.entity.AddressBook;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 地址管理 Mapper 接口
 * </p>
 *
 * @author zyw
 * @since 2023-06-16
 */
public interface AddressBookMapper extends BaseMapper<AddressBook> {

}
