package com.bdqn.mapper;

import com.bdqn.entity.DishFlavor;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 菜品口味关系表 Mapper 接口
 * </p>
 *
 * @author zyw
 * @since 2023-06-08
 */
public interface DishFlavorMapper extends BaseMapper<DishFlavor> {

}
