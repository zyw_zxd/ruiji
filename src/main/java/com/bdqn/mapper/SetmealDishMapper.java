package com.bdqn.mapper;

import com.bdqn.entity.SetmealDish;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 套餐菜品关系 Mapper 接口
 * </p>
 *
 * @author zyw
 * @since 2023-06-20
 */
public interface SetmealDishMapper extends BaseMapper<SetmealDish> {

}
