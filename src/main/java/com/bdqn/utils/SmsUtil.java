package com.bdqn.utils;

import org.apache.http.HttpResponse;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * @author:zyw
 * @create:2023-06-12 16:19
 */
public class SmsUtil {
   public static void sendMessage(String phone,String param){
       Map<String, String> headers = new HashMap<String, String>();
       headers.put("Authorization", "APPCODE " + "86ff2af7f9e94dd7a11eb071e377fdc6");
       //根据API的要求，定义相对应的Content-Type
       headers.put("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
       //需要给X-Ca-Nonce的值生成随机字符串，每次请求不能相同
       headers.put("X-Ca-Nonce", UUID.randomUUID().toString());
       Map<String, String> querys = new HashMap<String, String>();
       //封装其他参数
       Map<String, String> bodys = new HashMap<String, String>();
       //手机号
       bodys.put("phoneNumber", phone);

       //签名id
       bodys.put("smsSignId", "0000");
       //默认模板id
       //bodys.put("smsTemplateNo", "smsTemplateNo");
       //自定义验证码
       bodys.put("verifyCode", param);
       bodys.put("error","消息发送异常");

       try {
           HttpResponse response = HttpUtils.doPost("https://miitangs09.market.alicloudapi.com", "/v1/tools/sms/code/sender", "post", headers, querys, bodys);
           int flag = response.getStatusLine().getStatusCode();

           if (flag==200){
               System.out.println("发送成功");

           }else{
               System.out.println("发送失败");
           }

       } catch (Exception e) {
           e.printStackTrace();
           System.out.println("发送失败");
       }
   }
   }

