package com.bdqn.utils;

/**
 * @author:zyw
 * @create:2023-06-07 21:11
 */
public class ThreadLocalUtil {
    private    static ThreadLocal<Long> local = new ThreadLocal<>();
    public static void setID(long id){
        local.set(id);
    }
    public static Long getID(){
        return local.get();
    }

}
